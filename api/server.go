package api

import (
	"fmt"
	"os"

	"golang.org/x/net/context"
)

type Server struct{}

var count int

func (s Server) HelloHanifa(ctx context.Context, req *HelloReq) (*HelloResp, error) {
	// time.Sleep(time.Millisecond * 500)
	count++
	fmt.Println("-----------------------------------")
	fmt.Println(req.Name)
	podName := os.Getenv("MY_POD_NAME")
	fmt.Println("This is server pod name:= ", podName)
	fmt.Println("-----------------------------------")

	return &HelloResp{Count: int32(count), PodnameC: podName}, nil
}
