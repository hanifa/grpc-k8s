# grpc-k8s

Step-By-Step instruction on how Haproxy load-balance between gRPC client and server. All docker images are built and hosted in `https://hub.docker.com/u/a8uhnf/`

1. Clone this repository into `$GOPATH/src/gitlab.com/hanifa/`

    ```
    $ mkdir -p $GOPATH/src/gitlab.com/hanifa/
    $ cd $GOPATH/src/gitlab.com/hanifa/
    $ git clone https://gitlab.com/hanifa/grpc-k8s.git
    $ cd grpc-k8s
    ```

2. Deploy server into your kubernetes cluster.

    ```
    $ kubectl apply -f k8s/server.yaml
    ```
    
    This command will deploy server into kubernetes cluster, also create a service for this server. You can find server code in `server/main.go`.

3. Now, run below commands are to deploy haproxy and haproxy-service

    ```
    $ kubectl apply -f haproxy/haproxy-deploy.yaml
    $ kubectl apply -f haproxy/haproxy-svc.yaml
    ```

4. Run below command to get `haproxy-service`'s `external-ip`

    ```
    $ kubectl get svc -w
    ```
    
    ```
    NAME              TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
    echo-server       ClusterIP      10.63.247.68   <none>        8080/TCP         56s
    haproxy-service   LoadBalancer   10.63.245.25   <pending>     8080:31342/TCP   5s
    kubernetes        ClusterIP      10.63.240.1    <none>        443/TCP          5d
    haproxy-service   LoadBalancer   10.63.245.25   35.185.223.98   8080:31342/TCP   50s
    ```
    
    Now, get this `haproxy-service` `external-ip` and replace the value of `haproxyIpAddress`(this is located in `client/config.yaml` file) with this `external-ip`
    

5. Now run client in local PC

    ```
    $ cd client
    $ go run main.go
    ```
    you can see  logs, from which server responses are coming from
    
    ```
    *********************
    I'll hit 35.185.223.98:8080 . 
    This address will proxy my call to grpc server. 
    *********************
    ---------------------
    Got reponse from echo-server-6b4d876db6-5dmt5 server. 
    This server is hit 1 times 
    ---------------------
    Got reponse from echo-server-6b4d876db6-tqtf5 server. 
    This server is hit 1 times 
    ---------------------
    Got reponse from echo-server-6b4d876db6-5dmt5 server. 
    This server is hit 2 times 
    ---------------------
    Got reponse from echo-server-6b4d876db6-5dmt5 server. 
    This server is hit 3 times 
    ---------------------
    Got reponse from echo-server-6b4d876db6-tqtf5 server. 
    This server is hit 2 times 
    ---------------------
    Got reponse from echo-server-6b4d876db6-tqtf5 server. 
    This server is hit 3 times 
    ---------------------
    Got reponse from echo-server-6b4d876db6-tqtf5 server. 
    This server is hit 4 times 
    ---------------------
    Got reponse from echo-server-6b4d876db6-tqtf5 server. 
    This server is hit 5 times 
    ---------------------
    Got reponse from echo-server-6b4d876db6-tqtf5 server. 
    This server is hit 6 times 
    ---------------------
    Got reponse from echo-server-6b4d876db6-5dmt5 server. 
    This server is hit 4 times 
    ---------------------
    ```