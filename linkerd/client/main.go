package main

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/hanifa/grpc-k8s/api"
	"google.golang.org/grpc"
)

func main() {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	cc, err := grpc.Dial(fmt.Sprintf("127.0.0.1:8080"), opts...)
	if err != nil {
		panic(nil)
	}
	defer cc.Close()

	c1 := time.Tick(time.Microsecond * 200)
	c2 := time.Tick(time.Second * 5)
	
	client := api.NewHelloHanifaClient(cc)


	countHit := 0
	countErr := 0
	countS := 0
	for {
		select {
		case <- c1:
			go func() {
				countHit++
				_, err := client.HelloHanifa(context.Background(), &api.HelloReq{Name: "shahriar"})
				if err != nil {
					countErr++
				} else {
					countS++
				}
			}()
		case <- c2:
			fmt.Printf("Hit: %v, Success: %v, Err: %v\n", countHit, countS, countErr)
		}
	}	
}
