# runs linkerd in a RC, in linker-to-linker mode, routing gRPC requests
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: l5d-config
data:
  config.yaml: |-
    admin:
      ip: 0.0.0.0
      port: 9990

    namers:
    - kind: io.l5d.k8s
      host: localhost
      port: 8001

    telemetry:
    - kind: io.l5d.prometheus
    - kind: io.l5d.recentRequests
      sampleRate: 0.25

    usage:
      orgId: linkerd-examples-replicationcontroller-grpc

    routers:
    - protocol: h2
      label: outgoing
      dtab: |
        /srv        => /#/io.l5d.k8s/default/grpc;
        /grpc       => /srv;
        /grpc/World => /srv/world-v1;
        /svc        => /$/io.buoyant.http.domainToPathPfx/grpc;
      identifier:
        kind: io.l5d.header.path
        segments: 1
      interpreter:
        kind: default
        transformers:
        - kind: io.l5d.k8s.localnode
      servers:
      - port: 4140
        ip: 0.0.0.0

    - protocol: h2
      label: incoming
      dtab: |
        /srv        => /#/io.l5d.k8s/default/grpc;
        /grpc       => /srv;
        /grpc/World => /srv/world-v1;
        /svc        => /$/io.buoyant.http.domainToPathPfx/grpc;
      identifier:
        kind: io.l5d.header.path
        segments: 1
      interpreter:
        kind: default
        transformers:
        - kind: io.l5d.k8s.localnode
      servers:
      - port: 4141
        ip: 0.0.0.0
---
kind: ReplicationController
apiVersion: v1
metadata:
  name: linkerd
spec:
  replicas: 3
  selector:
    app: linkerd
  template:
    metadata:
      labels:
        app: linkerd
    spec:
      containers:
      - name: l5d
        image: buoyantio/linkerd:1.3.6
        env:
        - name: POD_IP
          valueFrom:
            fieldRef:
              fieldPath: status.podIP
        args:
        - /io.buoyant/linkerd/config/config.yaml
        ports:
        - name: outgoing
          containerPort: 4140
        - name: incoming
          containerPort: 4141
        - name: admin
          containerPort: 9990
        volumeMounts:
        - name: "linkerd-config"
          mountPath: "/io.buoyant/linkerd/config"
          readOnly: true
        # We run a kubectl proxy container so that linkerd can talk to the
        # Kubernetes master API.
      - name: kubectl
        image: buoyantio/kubectl:v1.8.5
        args:
        - proxy
        - "-p"
        - "8001"
      volumes:
      - name: linkerd-config
        configMap:
          name: l5d-config
---
apiVersion: v1
kind: Service
metadata:
  name: l5d
spec:
  selector:
    app: linkerd
  type: LoadBalancer
  ports:
  - name: outgoing
    port: 4140
  - name: incoming
    port: 4141
  - name: admin
    port: 9990
