package main

import (
	"context"
	"fmt"
	"os"
	"time"

	"io/ioutil"

	"sync"

	"github.com/ghodss/yaml"
	"gitlab.com/hanifa/grpc-k8s/api"
	"google.golang.org/grpc"
)

func dieIf(err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}

type config struct {
	HaproxyIPAddress string `json:"haproxyIpAddress"`
}

var l sync.Mutex
var el sync.Mutex

func main() {
	podname := os.Getenv("MY_POD_NAME")
	file, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		panic(err)
	}
	cfg := &config{}

	err = yaml.Unmarshal(file, cfg)
	if err != nil {
		panic(err)
	}

	fmt.Println("*********************")
	fmt.Printf("I'll hit %s . \nThis address will proxy my call to grpc server. \n", fmt.Sprintf("%s:8080", cfg.HaproxyIPAddress))
	fmt.Println("*********************")
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	cc, err := grpc.Dial(fmt.Sprintf("app-1.app:80"), opts...)
	if err != nil {
		panic(nil)
	}

	defer cc.Close()

	c := time.Tick(time.Millisecond * 1)
	client := api.NewHelloHanifaClient(cc)


	start := make(chan *bool)

	no := 10000

	wg := new(sync.WaitGroup)
	wg.Add(no)

	count := 0
	for range c {

		count++
		if count > 10000 {
			break
		}

		//fmt.Println("---- Hitting ", count, res)
		go func(wg *sync.WaitGroup) {
			<-start
			client.HelloHanifa(context.Background(), &api.HelloReq{Name: "shahriar", Podname: podname})
			//fmt.Println(resp.Count, resp.PodnameC)
			wg.Done()

		}(wg)
	}

	//time.Sleep(time.Second * 1)

	ss := time.Now()
	close(start)
	wg.Wait()

	fmt.Println(time.Since(ss).String())


	time.Sleep(time.Hour)

}
