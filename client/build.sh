#!/usr/bin/env bash

rm grpc_k8s_client
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o grpc_k8s_client main.go
docker build -t a8uhnf/grpc_k8s_client:1.0 .
rm grpc_k8s_client
docker push a8uhnf/grpc_k8s_client:1.0