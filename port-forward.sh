#!/bin/bash

name=$(kubectl get pods -n test -o name)
pod=""
for i in $name; do
    pod=${i#*/}  
done

kubectl port-forward $pod 9901:9901 -n test